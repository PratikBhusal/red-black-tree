import project.RedBlackTree;
import java.util.Random;

public class Project
{
    public static void main(String[] args) {
        Random random = new Random();

        RedBlackTree<Integer> intRBTree = new RedBlackTree<>();
        int temp;
        for(int i = 0; i < 10; i++){
            temp = random.nextInt(100);
            System.out.println("Random Value: " + temp);
            intRBTree.insert(new Integer(temp));
        }

        System.out.println("\nFull Integer Tree: ");
        System.out.println(intRBTree);

        RedBlackTree<Double> doubleRBTree = new RedBlackTree<>();
        double temp2;
        for (int i = 0; i < 10; ++i) {
            temp2 = random.nextDouble();
            doubleRBTree.insert(new Double(temp2));
        }

        System.out.println("\nFull Double Tree");
        System.out.println(doubleRBTree);
    }
}
