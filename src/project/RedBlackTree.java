package project;

public class RedBlackTree< E extends Comparable<E> >//{{{
{
    private static class Node< E extends Comparable<E> >{//{{{

        boolean color;
        E       element;
        Node<E> leftChild;
        Node<E> parent;
        Node<E> rightChild;

        Node(E element, boolean color) {
            this.color   = color;
            this.element = element;
            leftChild    = null;
            parent       = null;
            rightChild   = null;
        }

    }//}}}

    private static final boolean BLACK = true;
    private static final boolean RED   = false;
    private Node<E>                      root;

    public  RedBlackTree() {//{{{
        root = null;
    }//}}}

    /**
     * Insert an element into the red black tree. Traverses and inserts like a
     * Binary Search Tree. After insertion, we fix the tree to meet the
     * requiements for a red-black tree.
     *
     * @param   E   The element we are inserting
     *
     * @throws NullPointerException If the user tries to insert a null element.
     *
     * @return  True if we successfully inserted the element. False if the
     * element already exists or we could not insert the element.
     */
    public boolean insert(E element) {//{{{
        // Throw a descriptive null pointer exception if the element to insert is null
        if (element == null) {
            throw new NullPointerException("Cannot insert null element to tree.");
        }

        Node<E> curr = root;
        Node<E> prev = null;
        int comparison = 0;

        while (curr != null) {
            prev = curr;
            comparison = element.compareTo(curr.element);
            if (comparison < 0) {
                curr = curr.leftChild;
            }
            else if (comparison > 0){
                curr = curr.rightChild;
            }
            else {
                return false;
            }
        }
        // If the tree is empty
        if (prev == null) {
            root = new Node<>(element, BLACK);
            return true;
        }

        comparison = element.compareTo(prev.element);
        Node<E> node = new Node<>(element, RED);
        node.parent = prev;
        // Insert as a right child
        if (comparison < 0) {
            prev.leftChild = node;
            fix(node);
        // Insert as a right child
        } else if (comparison > 0) {
            prev.rightChild = node;
            fix(node);
        // If the element already exists, return false.
        } else {
            return false;
        }
        return true;
    }//}}}

    private Node<E> rotateLeft(Node<E> node1) {//{{{
        Node<E> node2 = node1.rightChild;
        node1.rightChild = node2.leftChild;
        node2.leftChild = node1;
        // Update the parent addresses for the nodes affected by rotation
        node1.parent = node2;
        if (node1.rightChild != null) node1.rightChild.parent = node1;
        node2.parent = null;
        return node2;
    }//}}}

    private Node<E> rotateRight(Node<E> node2) {//{{{
        Node<E> node1 = node2.leftChild;
        node2.leftChild = node1.rightChild;
        node1.rightChild = node2;
        // Update the parent addresses for the nodes affected by rotation
        node2.parent = node1;
        if (node2.leftChild != null) node2.leftChild.parent = node2;
        node1.parent = null;
        return node1;
    }//}}}

    private void rotateLeftLeft(Node<E> parent, Node<E> grandparent, Node<E> ancestor) {//{{{
        parent = rotateRight(grandparent);

        if (ancestor == null) {
            root = parent;
        }
        else {
            if (ancestor.element.compareTo(parent.element) > 0) {
                ancestor.leftChild = parent;
            } else {
                ancestor.rightChild = parent;
            }
            parent.parent = ancestor;
        }
        parent.color = BLACK;
        grandparent.color = RED;
    }//}}}

    private void rotateLeftRight(Node<E> node, Node<E> parent, Node<E> grandparent, Node<E> ancestor) {//{{{
        grandparent.leftChild = rotateLeft(parent);
        node = rotateRight(grandparent);

        if (ancestor == null) {
            root = node;
        }
        else {
            if (ancestor.element.compareTo(node.element) > 0) {
                ancestor.leftChild = node;
            } else {
                ancestor.rightChild = node;
            }
            node.parent = ancestor;
        }

        node.color = BLACK;
        grandparent.color = RED;
    }//}}}

    private void rotateRighttLeft(Node<E> node, Node<E> parent, Node<E> grandparent, Node<E> ancestor) {//{{{
        grandparent.rightChild = rotateRight(parent);
        node = rotateLeft(grandparent);

        if (ancestor == null) {
            root = node;
        }
        else {
            if (ancestor.element.compareTo(node.element) > 0) {
                ancestor.leftChild = node;
            }
            else {
                ancestor.rightChild = node;
            }
            node.parent = ancestor;
        }

        node.color = BLACK;
        grandparent.color = RED;
    }//}}}

    private void rotateRightRight(Node<E> parent, Node<E> grandparent, Node<E> ancestor) {//{{{
        parent = rotateLeft(grandparent);
        if (ancestor == null) {
            root = parent;
        }
        else {
            if (ancestor.element.compareTo(parent.element) > 0) {
                ancestor.leftChild = parent;
            }
            else {
                ancestor.rightChild = parent;
            }
            parent.parent = ancestor;
        }
        parent.color = BLACK;
        grandparent.color = RED;
    }//}}}

    private void recolor(Node<E> parent, Node<E> uncle, Node<E> grandparent) {//{{{
        parent.color = BLACK;
        uncle.color = BLACK;
        grandparent.color = RED;
    }//}}}

    private void fix(Node<E> node) {//{{{
        if (root.color == RED) {
            root.color = BLACK;
            return;
        }
        // If the node have no grandparent or we don't have a double red
        // situation, we have nothing to fix :D
        if (node.parent.parent == null || node.parent.color != RED) {
            return;
        }

        // Init Nodes we need for recoloring and/or restructing
        Node<E> parent = node.parent;
        Node<E> uncle = (node.parent.parent.element.compareTo(node.parent.element) < 0)
            ? node.parent.parent.leftChild
            : node.parent.parent.rightChild;
        Node<E> grandparent = node.parent.parent;
        Node<E> ancestor = (node.parent.parent.parent != null)
            ? node.parent.parent.parent
            : null;

        // If uncle is null or the color is black, restructure tree
        if (uncle == null || uncle.color == BLACK) {
            // Left - * Rotation Cases
            if ( parent.equals(grandparent.leftChild) ) {
                // Left - Left Rotation Case
                if (node.equals(parent.leftChild)) { // Left-left case
                    rotateLeftLeft(parent, grandparent, ancestor);
                }
                 // Left - Right Rotation Rase
                else {
                    rotateLeftRight(node, parent, grandparent, ancestor);
                }
            }
            // Right - * Rotation Cases
            else {
                // Right - Left Rotation Case
                if (node.equals(parent.leftChild)) {
                    rotateRighttLeft(node, parent, grandparent, ancestor);
                }
                // Right - Right Rotation Case
                else {
                    rotateRightRight(parent, grandparent, ancestor);
                }
            }
        }
        // Recolor Tree. Fix any new issues that appear at grandparent node or
        // above.
        else {
            recolor(parent, uncle, grandparent);
            fix(grandparent);
        }
    }//}}}

    /**
     * Determine if the tree contains the given element argument.
     *
     * @param   element The element we are looking for in the Tree
     *
     * @return  True if the element exists in the tree. Otherwise, false.
     *
     * Defaults to false if the tree is empty.
     */
    public boolean contains(E element) {//{{{
        Node<E> current = root;

        while (current.element != null) {
            if (element.compareTo(current.element) > 0) {
                current = current.rightChild;
            }
            else if (element.compareTo(current.element) < 0) {
                current = current.leftChild;
            }
            else {
                // element was found
                return true;
            }
        }

        // Element was not found
        return false;

    }//}}}

    private String inOrderTraversal(Node<E> node) {//{{{
        if (node == null) {
            return "";
        }
        String str = ""; // Instantiate the string to return

        // Traverse Left Side
        str += inOrderTraversal(node.leftChild);

        str += "\t";
        str += (node.color == RED) ? "*" : "";
        str += node.element.toString();

        // Traverse Right side
        str += inOrderTraversal(node.rightChild);

        return str;
    }//}}}
    /**
     *  Print the elements in the tree in order. String starts at index 1 to
     *  remove the initial tab generated by the recurive drive function
     *
     *  @return The element of the tree in order.
     */
    public String toString() {//{{{
        // returns substring from index 1. We do this to remove the initial tab
        // character.
        return inOrderTraversal(root).substring(1);
    }//}}}


}//}}}
